package angelectro.com.gooddrivers.view;

import android.support.annotation.NonNull;

import angelectro.com.gooddrivers.UserChoiceEnum;
import angelectro.com.gooddrivers.content.RegistrationNumber;

/**
 * Created by Zahit Talipov on 20.05.2016.
 */
public interface BaseView {

    void startAboutActivity(@NonNull RegistrationNumber registrationNumber);

    void startChoiceActionActivity(@NonNull UserChoiceEnum choiceEnum, @NonNull RegistrationNumber registrationNumber);
}
