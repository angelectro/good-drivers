package angelectro.com.gooddrivers.view;

import java.util.List;

import angelectro.com.gooddrivers.content.Action;

/**
 * Created by Zahit Talipov on 11.05.2016.
 */
public interface AboutDriverView {
    void showResult();

    void showLoading();

    void hideLoading();

    void showError();
}
