package angelectro.com.gooddrivers.view;

import java.util.List;

import angelectro.com.gooddrivers.content.Region;

/**
 * Created by Zahit Talipov on 11.05.2016.
 */
public interface ChoiceRegionView {
    void showListRegions(List<Region> regions);

    void showLoading();

    void hideLoading();

    void showError();
}
