package angelectro.com.gooddrivers.view;

/**
 * Created by Zahit Talipov on 30.03.2016.
 */
public interface ViewEvaluationDriver {
    public void wrongFormatOfNumber();
    public void openAboutScreen();

}
