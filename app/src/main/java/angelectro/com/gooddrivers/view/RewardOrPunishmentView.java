package angelectro.com.gooddrivers.view;

import java.util.List;

import angelectro.com.gooddrivers.content.Action;

/**
 * Created by Zahit Talipov on 06.04.2016.
 */
public interface RewardOrPunishmentView {

    void updateViewPager(List<Action> itemRPs);

    void showLoading();

    void hideLoading();

    void showError();
}
