package angelectro.com.gooddrivers.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKRequest;

import angelectro.com.gooddrivers.R;
import angelectro.com.gooddrivers.Settings;
import angelectro.com.gooddrivers.listeners.SignInCallback;

/**
 * Created by Zahit Talipov on 05.05.2016.
 */
public class SignInDialog extends DialogFragment implements DialogInterface.OnClickListener {
    String TAG = SignInDialog.class.getSimpleName();
    VKCallback<VKAccessToken> tokenVKCallback;
    CallbackManager callbackManager;
    int loginID;
    SignInCallback signInCallback;
    CharSequence[] list = {"Вконтакте", "Facebook"};

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        signInCallback = (SignInCallback) getActivity();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.signInDialogTitle)
                .setSingleChoiceItems(list, 0, this)
                .setCancelable(false)
                .setNegativeButton(R.string.textSignInNegativeButton, this);
        return builder.create();
    }

    public void show(FragmentManager manager) {
        show(manager, TAG);
    }

    public void hide() {
        dismiss();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (loginID) {
            case 0: {
                Log.d("social", ""+loginID);
                VKSdk.onActivityResult(requestCode, resultCode, data, tokenVKCallback);
                break;
            }
            case 1: {
                callbackManager.onActivityResult(requestCode, resultCode, data);
                break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
        hide();
    }

    private void signInVk() {
        tokenVKCallback = new VKCallback<VKAccessToken>() {
            @Override
            public void onResult(VKAccessToken res) {
                VKRequest vkRequest = VKApi.users().get();
                Settings.saveUser(res);
                signInCallback.signIn();
                Log.d("result", res.accessToken);
            }

            @Override
            public void onError(VKError error) {
                Log.d("error", error.errorMessage);
            }
        };
        VKSdk.login(this, null);
    }

    private void singInFB() {
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        Settings.saveUser(loginResult);
                        signInCallback.signIn();
                        Log.d("result", loginResult.getAccessToken().getToken());
                    }

                    @Override
                    public void onCancel() {

                    }

                    @Override
                    public void onError(FacebookException error) {
                        Log.d("result", error.getLocalizedMessage());
                    }
                });
        LoginManager.getInstance().logInWithReadPermissions(this, null);
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        loginID = which;
        switch (which) {
            case 0: {
                signInVk();
                break;
            }
            case 1: {
                singInFB();
                break;
            }
            case DialogInterface.BUTTON_NEGATIVE: {
                dismiss();
            }
        }
    }
}
