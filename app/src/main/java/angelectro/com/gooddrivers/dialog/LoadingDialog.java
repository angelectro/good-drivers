package angelectro.com.gooddrivers.dialog;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.afollestad.materialdialogs.MaterialDialog;

import angelectro.com.gooddrivers.R;

/**
 * Created by Zahit Talipov on 06.04.2016.
 */
public class LoadingDialog extends DialogFragment implements DialogInterface.OnClickListener {
    private static final java.lang.String TEXT_KEY = "text";
    private String TAG = LoadingDialog.class.getSimpleName();


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCancelable(false);

    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        String text = getString(R.string.loading_progress);
        return new MaterialDialog.Builder(getActivity())
                .progress(true, 0)
                .content(text)
                .build();
    }


    public void show(@NonNull FragmentManager manager) {
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.add(this, null);
        transaction.commitAllowingStateLoss();
    }


    @Override
    public void onClick(DialogInterface dialog, int which) {
        dismiss();
    }
}
