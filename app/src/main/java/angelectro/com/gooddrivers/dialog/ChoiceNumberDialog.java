package angelectro.com.gooddrivers.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

import angelectro.com.gooddrivers.content.RegistrationNumber;

/**
 * Created by Zahit Talipov on 18.05.2016.
 */
public class ChoiceNumberDialog extends DialogFragment implements DialogInterface.OnClickListener {
    private static final String TAG = ChoiceNumberDialog.class.getSimpleName();
    private static String DATA = "items";
    ChoiceNumberListener numberListener;
    ArrayList<String> numbers;

    public static ChoiceNumberDialog create(List<RegistrationNumber> numbers) {
        Bundle bundle = new Bundle();
        ArrayList<String> strings = new ArrayList<>();
        for (RegistrationNumber number : numbers) {
            strings.add(String.format("%s %s", number.getNumber(), number.getRegion()).toUpperCase());
        }
        bundle.putStringArrayList(DATA, strings);
        ChoiceNumberDialog choiceNumberDialog = new ChoiceNumberDialog();
        choiceNumberDialog.setArguments(bundle);
        return choiceNumberDialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        numberListener = (ChoiceNumberListener) getActivity();
        numbers = getArguments().getStringArrayList(DATA);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        if (!numbers.isEmpty()) {
            builder
                    .setSingleChoiceItems(numbers.toArray(new CharSequence[numbers.size()]), 0, this)
                    .setTitle("Выберите номер машины");
        } else {
            builder.setTitle("Номер не распознан")
                    .setMessage("Рекомендация: постарайтесь сделать более качественное фото")
                    .setPositiveButton("Ок", this);
        }
        return builder.create();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        if (!numbers.isEmpty()) {
            numberListener.result(RegistrationNumber.create(numbers.get(which)));
        }
        dismiss();
    }

    public void show(FragmentManager manager) {
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.add(this, null);
        transaction.commitAllowingStateLoss();
    }

    public interface ChoiceNumberListener {
        void result(RegistrationNumber registrationNumber);
    }
}
