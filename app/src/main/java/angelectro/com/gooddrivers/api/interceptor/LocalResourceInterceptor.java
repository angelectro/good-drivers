package angelectro.com.gooddrivers.api.interceptor;

import android.content.Context;

import java.io.IOException;
import java.io.InputStream;
import java.net.URLConnection;

import angelectro.com.gooddrivers.UserChoiceEnum;
import angelectro.com.gooddrivers.api.GoodDriversService;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.Protocol;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.Buffer;

/**
 * Created by Zahit Talipov on 07.04.2016.
 */
public class LocalResourceInterceptor
        implements Interceptor {

    Context context;

    public LocalResourceInterceptor(Context context) {
        this.context = context;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        String category = request.url().queryParameter(GoodDriversService.QUERY_CATEGORY);
        int resourcesId = 0;
        resourcesId = context.getResources().getIdentifier("static_data", "raw", context.getPackageName());
       /* switch (UserChoiceEnum.valueOf(category)) {
            case Reward: {
                resourcesId = context.getResources().getIdentifier("static_good", "raw", context.getPackageName());
                break;
            }
            case Punishment: {
                resourcesId = context.getResources().getIdentifier("static_bad", "raw", context.getPackageName());
                break;
            }
        }*/

        InputStream inputStream = context.getResources().openRawResource(resourcesId);
        String mimeType = URLConnection.guessContentTypeFromStream(inputStream);
        if (mimeType == null) {
            mimeType = "application/json";
        }
        Buffer buffer = new okio.Buffer().readFrom(inputStream);
        return new Response.Builder()
                .request(request)
                .protocol(Protocol.HTTP_1_1)
                .code(200)
                .body(ResponseBody.create(MediaType.parse(mimeType), buffer.size(), buffer)).build();
    }
}
