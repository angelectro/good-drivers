package angelectro.com.gooddrivers.api;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by Zahit Talipov on 12.05.2016.
 */
public interface ScanPhotoService {
    @Multipart
    @POST("uploadimage")
    public Call<ResponseBody> uploadImage(@Part MultipartBody.Part part);
}
