package angelectro.com.gooddrivers.api;

import android.content.Context;
import android.support.annotation.NonNull;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Zahit Talipov on 07.04.2016.
 */
public class ApiFactory {


    public static GoodDriversService getDriversService(@NonNull Context context) {
        return new Retrofit.Builder()
                .baseUrl("http://5.135.111.150:8093/")
                .client(OkHttp.createLocalClient(context))
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build().create(GoodDriversService.class);
    }

    public static ScanPhotoService getScanPhotoService() {
        return new Retrofit.Builder()
                .baseUrl("http://212.116.121.70:10000")
                .client(OkHttp.createHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build().create(ScanPhotoService.class);
    }
}
