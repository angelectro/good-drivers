package angelectro.com.gooddrivers.api;

import java.util.List;

import angelectro.com.gooddrivers.content.Action;
import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Zahit Talipov on 07.04.2016.
 */
public interface GoodDriversService {
    public static String QUERY_CATEGORY="category";

    @GET("all")
    public Observable<List<Action>> getListRawOrPan();

    @Multipart
    @POST("uploadimage")
    public Call<ResponseBody> uploadImage(@Part MultipartBody.Part part);

    @PUT("comment")
    public Call<ResponseBody> sendComment(@Body Action action);
}
