package angelectro.com.gooddrivers.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;

import angelectro.com.gooddrivers.database.table.BaseTable;

/**
 * Created by Zahit Talipov on 13.04.2016.
 */
public class DBHelper extends SQLiteOpenHelper {
    private static int DB_VERSION = 1;
    private static String DB_NAME = "good_driver.db";
    private Schema mSchema;

    public DBHelper(@NonNull Context context, @NonNull Schema schema) {
        super(context, DB_NAME, null, DB_VERSION);
        mSchema = schema;
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        for (BaseTable table : mSchema.getTables()) {
            table.create(db);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //nothing
    }
}
