package angelectro.com.gooddrivers.database;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;

import angelectro.com.gooddrivers.database.table.ActionTable;
import angelectro.com.gooddrivers.database.table.RegionTable;

/**
 * Created by Zahit Talipov on 18.04.2016.
 */
public class SQLContentProvider extends ContentProvider {

    public static final String sCONTENT_AUTHORITY = "angelectro.com.gooddrivers";
    public static final Uri sBASE_URI = Uri.parse("content://" + sCONTENT_AUTHORITY);
    private DBHelper dbHelper;
    private Schema mSchema;

    @Override
    public boolean onCreate() {
        Log.d("provider", "create");
        mSchema = new Schema();
        mSchema.registrTable(new ActionTable());
        mSchema.registrTable(new RegionTable());
        dbHelper = new DBHelper(getContext(), mSchema);
        return true;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Log.d("provider", "query");
        String table = getType(uri);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        if (!TextUtils.isEmpty(table)) {
            return database.query(table, projection, selection, selectionArgs, null, null, sortOrder);
        } else {
            throw new SQLException("No such table to query");
        }
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        return mSchema.findTable(uri);
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        String table = getType(uri);
        long id;
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        if (!TextUtils.isEmpty(table)) {
            id = database.insertWithOnConflict(table, null, values, SQLiteDatabase.CONFLICT_REPLACE);
            Uri uriResult = ContentUris.withAppendedId(uri, id);
            getContext().getContentResolver().notifyChange(uriResult, null);
            return uriResult;
        } else {
            throw new SQLException("No such table to query");
        }
    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] values) {
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        String table = getType(uri);
        if (TextUtils.isEmpty(table)) {
            throw new SQLiteException("No such table to insert");
        } else {
            int numInserted = 0;
            database.beginTransaction();
            try {
                for (ContentValues contentValues : values) {
                    long id = database.insertWithOnConflict(table, null, contentValues, SQLiteDatabase.CONFLICT_REPLACE);
                    if (id > 0) {
                        numInserted++;
                    }
                }

                getContext().getContentResolver().notifyChange(uri, null);
                database.setTransactionSuccessful();
            } finally {
                database.endTransaction();
            }
            return numInserted;
        }
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        String table = getType(uri);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        if (!TextUtils.isEmpty(table)) {
            return database.delete(table, selection, selectionArgs);
        } else {
            throw new SQLException("No such table to query");
        }
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        String table = getType(uri);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        if (!TextUtils.isEmpty(table)) {
            return database.update(table, values, selection, selectionArgs);
        } else {
            throw new SQLException("No such table to query");
        }
    }
}
