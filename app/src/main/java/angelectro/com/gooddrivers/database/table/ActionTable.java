package angelectro.com.gooddrivers.database.table;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;

import java.util.Iterator;
import java.util.List;

import angelectro.com.gooddrivers.content.Action;

/**
 * Created by Zahit Talipov on 19.04.2016.
 */
public class ActionTable extends BaseTable<Action> {
    public static String CATEGORY = "category";
    public static String DESCRIPTION = "description";
    public static String IMAGE_URL = "image";
    public static String TYPE = "type"; //good or bad

    @Override
    public void create(SQLiteDatabase sqLiteDatabase) {
        TableBuilder.create(this)
                .primaryKey()
                .stringColumn(CATEGORY)
                .stringColumn(DESCRIPTION)
                .stringColumn(IMAGE_URL)
                .stringColumn(TYPE)
                .execute(sqLiteDatabase);
    }

    @Override
    public ContentValues toValues(@NonNull Action action) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(CATEGORY, action.getCategory());
        contentValues.put(DESCRIPTION, action.getDescription());
        contentValues.put(IMAGE_URL, action.getUrl_image());
        contentValues.put(TYPE, action.getType());
        return contentValues;
    }

    @Override
    public ContentValues[] toValuesArray(@NonNull List actions) {
        ContentValues[] contentValues = new ContentValues[actions.size()];
        Iterator<Action> iterator = actions.iterator();
        int i = 0;
        while (iterator.hasNext()) {
            contentValues[i] = toValues(iterator.next());
            i++;
        }
        return contentValues;
    }

    @Override
    public Action fromCursor(@NonNull Cursor cursor) {
        Action action = new Action(
                cursor.getString(cursor.getColumnIndex(ActionTable.TYPE)),
                cursor.getString(cursor.getColumnIndex(ActionTable.IMAGE_URL)),
                cursor.getString(cursor.getColumnIndex(ActionTable.CATEGORY)),
                cursor.getString(cursor.getColumnIndex(ActionTable.DESCRIPTION)));
        return action;
    }


}
