package angelectro.com.gooddrivers.database.table;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;

import java.util.List;

import angelectro.com.gooddrivers.content.Action;
import angelectro.com.gooddrivers.database.SQLContentProvider;

/**
 * Created by Zahit Talipov on 18.04.2016.
 */
public abstract class BaseTable<T> {

    public static String ID = "_id";

    public Uri getUri() {
        return SQLContentProvider.sBASE_URI.buildUpon().appendPath(getTableName()).build();
    }

    public String getTableName() {
        return getClass().getSimpleName();
    }

    public abstract void create(SQLiteDatabase sqLiteDatabase);

    public abstract ContentValues toValues(@NonNull T t);
    public abstract ContentValues[] toValuesArray(@NonNull List list);
    public abstract T fromCursor(@NonNull Cursor cursor);
}
