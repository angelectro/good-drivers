package angelectro.com.gooddrivers.database;

import android.content.Context;
import android.database.Cursor;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import angelectro.com.gooddrivers.UserChoiceEnum;
import angelectro.com.gooddrivers.content.Action;
import angelectro.com.gooddrivers.content.Region;
import angelectro.com.gooddrivers.database.rx.CursorListMapper;
import angelectro.com.gooddrivers.database.rx.CursorObservable;
import angelectro.com.gooddrivers.database.table.ActionTable;
import angelectro.com.gooddrivers.database.table.RegionTable;
import rx.Observable;
import rx.functions.Func1;

/**
 * Created by Zahit Talipov on 19.04.2016.
 */
public class SQLite {

    private static Context mContext;

    public static void initialize(@NonNull Context context) {
        mContext = context;
    }

    public static Observable<List<Action>> getAllActionsObservable() {
        ActionTable actionTable = new ActionTable();
        return new CursorObservable(mContext,
                actionTable.getUri(),
                null,
                null,
                null, null)
                .map(new CursorListMapper(new Func1<Cursor, Action>() {
                    @Override
                    public Action call(Cursor cursor) {
                        return actionTable.fromCursor(cursor);
                    }
                }));
    }

    public static List<Action> getAllActions(UserChoiceEnum choiceEnum) {
        List<Action> actions = new ArrayList<>();
        ActionTable actionTable = new ActionTable();
        Cursor cursor = mContext.getContentResolver().query(
                new ActionTable().getUri(),
                null,
                "type = " + choiceEnum.name(),
                null,
                null);
        while (cursor.moveToNext()) {
            actionTable.fromCursor(cursor);
        }
        return actions;
    }

    public static List<Action> getAllActions(String description) {
        List<Action> actions = new ArrayList<>();
        ActionTable actionTable = new ActionTable();
        Cursor cursor = mContext.getContentResolver().query(
                new ActionTable().getUri(),
                null,
                "description = '" + description + "'",
                null,
                null);
        while (cursor.moveToNext()) {
            actions.add(actionTable.fromCursor(cursor));
        }
        return actions;
    }

    public static void saveActions(List<Action> actions) {
        ActionTable actionTable = new ActionTable();
        mContext.getContentResolver().bulkInsert(actionTable.getUri(), actionTable.toValuesArray(actions));
    }

    public static void cleanActions() {
        ActionTable actionTable = new ActionTable();
        mContext.getContentResolver().delete(actionTable.getUri(), null, null);
    }

    public static Observable<List<Region>> getAllRegionsObservable() {
        RegionTable actionTable = new RegionTable();
        return new CursorObservable(mContext,
                actionTable.getUri(),
                null,
                null,
                null, null)
                .map(new CursorListMapper(new Func1<Cursor, Region>() {
                    @Override
                    public Region call(Cursor cursor) {
                        return actionTable.fromCursor(cursor);
                    }
                }));
    }

    public static void saveRegions(List<Region> actions) {
        RegionTable regionTable = new RegionTable();
        mContext.getContentResolver().bulkInsert(regionTable.getUri(), regionTable.toValuesArray(actions));
    }

    public static void cleanRegions() {
        RegionTable regionTable = new RegionTable();
        mContext.getContentResolver().delete(regionTable.getUri(), null, null);
    }
}
