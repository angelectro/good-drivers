package angelectro.com.gooddrivers.database.rx;

import android.database.Cursor;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import rx.functions.Func1;

/**
 * @author Artur Vasilov
 */
public class CursorListMapper<T> implements Func1<Cursor, List<T>> {

    private final Func1<Cursor, T> mTransformFunc;

    public CursorListMapper(@NonNull Func1<Cursor, T> transformFunc) {
        mTransformFunc = transformFunc;
    }

    @Override
    public List<T> call(Cursor cursor) {
        List<T> list = new ArrayList<>();
        if (cursor.isClosed()) {
            return list;
        }
        while (cursor.moveToNext()) {
            list.add(mTransformFunc.call(cursor));
        }
        return list;
    }
}
