package angelectro.com.gooddrivers.database;

import android.content.UriMatcher;
import android.net.Uri;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import angelectro.com.gooddrivers.database.table.BaseTable;

/**
 * Created by Zahit Talipov on 19.04.2016.
 */
public class Schema {

    private UriMatcher mUriMatcher;
    private List<BaseTable> mTables;

    public Schema() {
        mTables= new ArrayList<>();
        mUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    }

    public List<BaseTable> getTables() {
        return mTables;
    }

    public void registrTable(@NonNull BaseTable baseTable) {
        mTables.add(baseTable);
        mUriMatcher.addURI(SQLContentProvider.sCONTENT_AUTHORITY, baseTable.getTableName(), mTables.size());
    }

    @NonNull
    public String findTable(@NonNull Uri uri) {
        return mTables.get(mUriMatcher.match(uri)-1).getTableName();
    }
}
