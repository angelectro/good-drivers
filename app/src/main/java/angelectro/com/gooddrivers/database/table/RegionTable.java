package angelectro.com.gooddrivers.database.table;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;

import java.util.Iterator;
import java.util.List;

import angelectro.com.gooddrivers.content.Region;

/**
 * Created by Zahit Talipov on 11.05.2016.
 */
public class RegionTable extends BaseTable<Region> {
    private String CODE = "code";
    private String REGION = "region";

    @Override
    public void create(SQLiteDatabase sqLiteDatabase) {
        TableBuilder.create(this)
                .primaryKey()
                .stringColumn(CODE)
                .stringColumn(REGION)
                .execute(sqLiteDatabase);

    }

    @Override
    public ContentValues toValues(@NonNull Region region) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(CODE, region.getCode());
        contentValues.put(REGION, region.getRegion());
        return contentValues;
    }

    @Override
    public ContentValues[] toValuesArray(@NonNull List list) {
        ContentValues[] contentValues = new ContentValues[list.size()];
        Iterator<Region> iterator = list.iterator();
        int i = 0;
        while (iterator.hasNext()) {
            contentValues[i] = toValues(iterator.next());
            i++;
        }
        return contentValues;
    }

    @Override
    public Region fromCursor(@NonNull Cursor cursor) {
        return new Region(
                cursor.getString(cursor.getColumnIndex(CODE)),
                cursor.getString(cursor.getColumnIndex(REGION))
        );
    }
}
