package angelectro.com.gooddrivers.database.rx;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import rx.Observable;
import rx.Subscriber;

/**
 * Created by Zahit Talipov on 19.04.2016.
 */
public class CursorObservable extends Observable<Cursor> {
    public CursorObservable(final Context context, @NonNull final Uri uri, final @Nullable String[] projection,
                            @Nullable final String selection, @Nullable final String[] selectionArgs,
                            @Nullable final String sortOrder) {
        super(new OnSubscribe<Cursor>() {
            @Override
            public void call(Subscriber<? super Cursor> subscriber) {
                Log.d("uri", uri.toString());
                Cursor cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, sortOrder);
                subscriber.onNext(cursor);
                subscriber.onCompleted();
            }
        });
    }
}
