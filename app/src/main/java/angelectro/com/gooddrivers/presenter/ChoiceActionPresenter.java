package angelectro.com.gooddrivers.presenter;

import android.content.Context;
import android.support.annotation.NonNull;

import angelectro.com.gooddrivers.UserChoiceEnum;
import angelectro.com.gooddrivers.content.Action;

/**
 * Created by Zahit Talipov on 06.05.2016.
 */
public interface ChoiceActionPresenter {
    public void getList(@NonNull Context context);
    public void update(@NonNull Context context);
    public void sendAction(@NonNull Action action, @NonNull String number);
}
