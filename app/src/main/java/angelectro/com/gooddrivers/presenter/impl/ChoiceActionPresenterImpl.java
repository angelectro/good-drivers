package angelectro.com.gooddrivers.presenter.impl;

import android.content.Context;
import android.support.annotation.NonNull;

import java.util.List;

import angelectro.com.gooddrivers.UserChoiceEnum;
import angelectro.com.gooddrivers.api.ApiFactory;
import angelectro.com.gooddrivers.content.Action;
import angelectro.com.gooddrivers.database.SQLite;
import angelectro.com.gooddrivers.presenter.ChoiceActionPresenter;
import angelectro.com.gooddrivers.view.RewardOrPunishmentView;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by Zahit Talipov on 06.04.2016.
 */
public class ChoiceActionPresenterImpl implements ChoiceActionPresenter {

    RewardOrPunishmentView mView;

    public ChoiceActionPresenterImpl(RewardOrPunishmentView view) {
        this.mView = view;
    }

    public void getList(@NonNull Context context) {
        SQLite.getAllActionsObservable()
                .doOnError(throwable -> mView.showError())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(list -> mView.updateViewPager(list));
    }

    public void update(@NonNull Context context) {
        ApiFactory.getDriversService(context)
                .getListRawOrPan()
                .flatMap(new Func1<List<Action>, Observable<List<Action>>>() {
                    @Override
                    public Observable<List<Action>> call(List<Action> actions) {
                        SQLite.cleanActions();
                        SQLite.saveActions(actions);
                        return Observable.just(actions);
                    }
                })
                .doOnSubscribe(mView::showLoading)
                .doOnTerminate(mView::hideLoading)
                .doOnError(throwable -> getList(context))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(list -> mView.updateViewPager(list));

    }

    @Override
    public void sendAction(@NonNull Action action, @NonNull String number
    ) {

    }
}
