package angelectro.com.gooddrivers.presenter;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import angelectro.com.gooddrivers.view.ViewEvaluationDriver;

/**
 * Created by Zahit Talipov on 30.03.2016.
 */
public class EvaluationDriverPresenter {

    ViewEvaluationDriver view;

    private String REGEX_OF_NUMBER = "[А-Яа-я][0-9]{3}[А-Яа-я]{2}";

    public EvaluationDriverPresenter(ViewEvaluationDriver view) {
        this.view = view;
    }

    public void checkNumberFormat(@NonNull String number) {
        if (!TextUtils.isEmpty(number))
            if (number.matches(REGEX_OF_NUMBER)) {
                view.openAboutScreen();
            } else {
                view.wrongFormatOfNumber();
            }
    }
}
