package angelectro.com.gooddrivers.presenter.impl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import angelectro.com.gooddrivers.FireBaseData;
import angelectro.com.gooddrivers.content.Action;
import angelectro.com.gooddrivers.database.SQLite;
import angelectro.com.gooddrivers.presenter.ChoiceActionPresenter;
import angelectro.com.gooddrivers.view.RewardOrPunishmentView;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Zahit Talipov on 06.05.2016.
 */
public class ChoiceActionPresenterFireBaseImpl implements ChoiceActionPresenter {

    RewardOrPunishmentView mView;

    public ChoiceActionPresenterFireBaseImpl(RewardOrPunishmentView view) {
        this.mView = view;
    }


    @Override
    public void getList(@NonNull Context context) {
        SQLite.getAllActionsObservable()
                .doOnSubscribe(() -> mView.showLoading())
                .doOnTerminate(() -> {
                    mView.hideLoading();
                    update(context);
                })
                .doOnError(throwable -> mView.showError())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(list -> mView.updateViewPager(list));
    }

    @Override
    public void update(@NonNull Context context) {
        FireBaseData.getActions().addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.d("firebase ", dataSnapshot.toString());
                List<Action> actions = new ArrayList<Action>();
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    Action action = data.getValue(Action.class);
                    actions.add(action);
                }
                SQLite.cleanActions();
                SQLite.saveActions(actions);
                mView.updateViewPager(actions);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                Log.d("firebase", firebaseError.getMessage());
            }
        });
    }

    public void sendAction(@NonNull Action action, @NonNull String number) {
        FireBaseData.getRegistrationNumbers().child(number).push().setValue(action, new Firebase.CompletionListener() {
            @Override
            public void onComplete(FirebaseError firebaseError, Firebase firebase) {
                Log.d("firebase", "complite");
            }
        });

    }
}
