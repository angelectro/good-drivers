package angelectro.com.gooddrivers.presenter.impl;

import com.firebase.client.DataSnapshot;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import angelectro.com.gooddrivers.FireBaseData;
import angelectro.com.gooddrivers.presenter.AboutDriverPresenter;
import angelectro.com.gooddrivers.view.AboutDriverView;

/**
 * Created by Zahit Talipov on 14.05.2016.
 */
public class AboutDriverPresenterFirebaseImpl implements AboutDriverPresenter {
    AboutDriverView aboutDriverView;

    public AboutDriverPresenterFirebaseImpl(AboutDriverView aboutDriverView) {
        this.aboutDriverView = aboutDriverView;
    }

    @Override
    public void findNumber(String number) {
        FireBaseData.getRegistrationNumbers().child(number)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onCancelled(FirebaseError firebaseError) {

                    }
                });
    }
}
