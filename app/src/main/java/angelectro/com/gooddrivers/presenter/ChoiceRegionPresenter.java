package angelectro.com.gooddrivers.presenter;

import com.firebase.client.DataSnapshot;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import angelectro.com.gooddrivers.FireBaseData;
import angelectro.com.gooddrivers.content.Region;
import angelectro.com.gooddrivers.database.SQLite;
import angelectro.com.gooddrivers.view.ChoiceRegionView;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Zahit Talipov on 11.05.2016.
 */
public class ChoiceRegionPresenter {

    ChoiceRegionView mChoiceRegionView;

    public ChoiceRegionPresenter(ChoiceRegionView choiceRegionView) {
        this.mChoiceRegionView = choiceRegionView;
    }

    public void getRegions() {
        SQLite.getAllRegionsObservable()
                .doOnSubscribe(() -> mChoiceRegionView.showLoading())
                .doOnTerminate(() -> {
                    mChoiceRegionView.hideLoading();
                    updateListRegions();
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError(Throwable::printStackTrace)
                .subscribe(regions -> {
                    if (regions.isEmpty())
                        mChoiceRegionView.showLoading();
                    else
                        mChoiceRegionView.showListRegions(regions);
                });
    }

    private void updateListRegions() {
        FireBaseData.getRegions().addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<Region> regions = new ArrayList<Region>();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Region region = snapshot.getValue(Region.class);
                    regions.add(region);
                }
                SQLite.cleanRegions();
                SQLite.saveRegions(regions);
                mChoiceRegionView.hideLoading();
                mChoiceRegionView.showListRegions(regions);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                mChoiceRegionView.hideLoading();
            }
        });
    }
}
