package angelectro.com.gooddrivers.presenter;

/**
 * Created by Zahit Talipov on 11.05.2016.
 */
public interface AboutDriverPresenter {

    public void findNumber(String number);
}
