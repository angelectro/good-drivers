package angelectro.com.gooddrivers;

import android.support.annotation.NonNull;

import com.firebase.client.Firebase;

import angelectro.com.gooddrivers.content.Action;

/**
 * Created by Zahit Talipov on 06.05.2016.
 */
public class FireBaseData {
    public static String BASE_URL = "https://good-drivers.firebaseio.com";
    private static String ACTIONS_URL = "/actions";
    private static String REGIONS_URL = "/regions";
    private static String REGISTRATION_NUMBERS_URL = "/registration_numbers";

    public static Firebase getActions() {
        return new Firebase(BASE_URL + ACTIONS_URL);
    }

    public static Firebase getRegions() {
        return new Firebase(BASE_URL + REGIONS_URL);
    }

    public static Firebase getRegistrationNumbers() {
        return new Firebase(BASE_URL + REGISTRATION_NUMBERS_URL);
    }

    public static FireBaseData getInstance() {
        return new FireBaseData();
    }

    public void findRegistrationNumber() {

    }

    ;

    public void sendAction(@NonNull Action action, @NonNull String number, @NonNull UserChoiceEnum choiceEnum) {

    }
}
