package angelectro.com.gooddrivers.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.List;

import angelectro.com.gooddrivers.R;
import angelectro.com.gooddrivers.adapters.RegionsAdapter;
import angelectro.com.gooddrivers.content.Region;
import angelectro.com.gooddrivers.dialog.LoadingDialog;
import angelectro.com.gooddrivers.listeners.OnItemClickRegion;
import angelectro.com.gooddrivers.presenter.ChoiceRegionPresenter;
import angelectro.com.gooddrivers.view.ChoiceRegionView;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Zahit Talipov on 11.05.2016.
 */
public class ChoiceRegionActivity extends BaseActivity implements ChoiceRegionView, OnItemClickRegion {
    public static String RESULT_REGION = "result";
    @BindView(R.id.regionsRV)
    RecyclerView recyclerView;
    RegionsAdapter regionsAdapter;
    ChoiceRegionPresenter presenter;
    LoadingDialog loadingDialog;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.regions_recycler_view);
        ButterKnife.bind(this);
        loadingDialog = new LoadingDialog();
        presenter = new ChoiceRegionPresenter(this);
        presenter.getRegions();
        regionsAdapter = new RegionsAdapter(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(regionsAdapter);
    }

    @Override
    public void showListRegions(List<Region> regions) {
        regionsAdapter.update(regions);
    }

    @Override
    public void showLoading() {
        loadingDialog.show(getFragmentManager());
    }

    @Override
    public void hideLoading() {
        loadingDialog.dismiss();
    }

    @Override
    public void showError() {

    }


    @Override
    public void onClickItem(Region region) {
        Intent intent = new Intent();
        intent.putExtra(RESULT_REGION, region);
        setResult(RESULT_OK, intent);
        finish();
    }

}
