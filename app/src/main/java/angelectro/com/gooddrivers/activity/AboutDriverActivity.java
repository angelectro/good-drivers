package angelectro.com.gooddrivers.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.TextView;

import angelectro.com.gooddrivers.AppDelegate;
import angelectro.com.gooddrivers.R;
import angelectro.com.gooddrivers.UserChoiceEnum;
import angelectro.com.gooddrivers.content.RegistrationNumber;
import angelectro.com.gooddrivers.dialog.LoadingDialog;
import angelectro.com.gooddrivers.presenter.AboutDriverPresenter;
import angelectro.com.gooddrivers.presenter.impl.AboutDriverPresenterFirebaseImpl;
import angelectro.com.gooddrivers.view.AboutDriverView;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Zahit Talipov on 27.03.2016.
 */
public class AboutDriverActivity extends BaseActivity implements AboutDriverView {

    private static String NUMBER = "number";
    @BindView(R.id.textViewNumber)
    TextView mTextViewNumber;
    @BindView(R.id.textViewRegion)
    TextView mTextViewRegion;
    LoadingDialog loadingDialog;
    AboutDriverPresenter presenter;
    String registrationNumber;

    public static void start(@NonNull Context context, @NonNull String registrationNumber) {
        Intent intent = new Intent(context, AboutDriverActivity.class);
        intent.putExtra(NUMBER, registrationNumber);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about_activity);
        ButterKnife.bind(this);
        registrationNumber = getIntent().getStringExtra(NUMBER);
        AppDelegate.currentRegistrationNumber = RegistrationNumber.create(registrationNumber);
        showRegistrationNumber();
        presenter = new AboutDriverPresenterFirebaseImpl(this);
        loadingDialog = new LoadingDialog();
        presenter.findNumber(registrationNumber);
    }

    public void onClickChoice(View view) {
        switch (view.getId()) {
            case R.id.buttonGood: {
                ChoiceActionActivity.startActivity(this, UserChoiceEnum.Reward, registrationNumber);
                break;
            }
            case R.id.buttonBad: {
                ChoiceActionActivity.startActivity(this, UserChoiceEnum.Punishment, registrationNumber);
                break;
            }
        }
    }

    void showRegistrationNumber() {
        RegistrationNumber number = RegistrationNumber.create(registrationNumber.toUpperCase());
        mTextViewNumber.setText(number.getNumber());
        mTextViewRegion.setText(number.getRegion());
    }

    @Override
    public void showResult() {

    }

    @Override
    public void showLoading() {
        loadingDialog.show(getFragmentManager());
    }

    @Override
    public void hideLoading() {
        loadingDialog.dismiss();
    }

    @Override
    public void showError() {

    }
}

