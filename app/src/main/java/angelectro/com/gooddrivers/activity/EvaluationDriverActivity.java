package angelectro.com.gooddrivers.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import angelectro.com.gooddrivers.R;
import angelectro.com.gooddrivers.content.Region;
import angelectro.com.gooddrivers.content.RegistrationNumber;
import angelectro.com.gooddrivers.dialog.ChoiceNumberDialog;
import angelectro.com.gooddrivers.dialog.LoadingDialog;
import angelectro.com.gooddrivers.listeners.CallbackResult;
import angelectro.com.gooddrivers.listeners.ScanImage;
import angelectro.com.gooddrivers.listeners.impl.ScanImageImpl;
import angelectro.com.gooddrivers.presenter.EvaluationDriverPresenter;
import angelectro.com.gooddrivers.view.ViewEvaluationDriver;
import angelectro.com.gooddrivers.voice_control.VoiceRecognition;
import butterknife.BindView;
import butterknife.ButterKnife;

public class EvaluationDriverActivity extends BaseActivity implements ViewEvaluationDriver, CallbackResult<List<RegistrationNumber>>{

    @BindView(R.id.etNumberOfPlate)
    EditText mETNumberOfPlate;
    @BindView(R.id.tvRegionOfPlate)
    TextView mTVRegionOFPlate;
    @BindView(R.id.buttonFind)
    Button buttonFind;
    @BindView(R.id.tvWrongFormat)
    TextView tvWrongFormat;
    ScanImage scanImage;
    LoadingDialog loadingDialog;

    EvaluationDriverPresenter evaluationDriverPresenter;
    private int REQUEST_CODE_REGION = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.evaluation_driver_activity);
        ButterKnife.bind(this);
        evaluationDriverPresenter = new EvaluationDriverPresenter(this);
        scanImage = new ScanImageImpl(this);
    }




    public void onClickFindDriver(View view) {
        if (!TextUtils.isEmpty(mTVRegionOFPlate.getText().toString()))
            evaluationDriverPresenter.checkNumberFormat(mETNumberOfPlate.getText().toString());
        else
            evaluationDriverPresenter.checkNumberFormat("А323АА");
        View viewFocus = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(viewFocus.getWindowToken(), 0);
        }
    }

    @Override
    public void wrongFormatOfNumber() {
        tvWrongFormat.setVisibility(View.VISIBLE);
    }

    @Override
    public void openAboutScreen() {
        tvWrongFormat.setVisibility(View.INVISIBLE);
        String number = mETNumberOfPlate.getText().toString().concat(" " + mTVRegionOFPlate.getText()).toLowerCase();
        AboutDriverActivity.start(this, number);
    }

    public void onClickChoiceRegion(View view) {
        startActivityForResult(new Intent(this, ChoiceRegionActivity.class), REQUEST_CODE_REGION);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (scanImage.onActivityResult(requestCode, resultCode, data, this)) {
            loadingDialog = new LoadingDialog();
            loadingDialog.show(getFragmentManager());
        }
        if (requestCode == REQUEST_CODE_REGION && resultCode == RESULT_OK) {
            Region region = (Region) data.getSerializableExtra(ChoiceRegionActivity.RESULT_REGION);
            mTVRegionOFPlate.setText(region.getCode());
        }
    }

    public void onClickPhoto(View view) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, ScanImageImpl.generateFileUri());
        startActivityForResult(intent, ScanImage.REQUEST_CODE_PHOTO);
    }

    @Override
    public void onResult(List<RegistrationNumber> registrationNumbers) {
        if (loadingDialog != null)
            loadingDialog.dismissAllowingStateLoss();
        if (registrationNumbers.size() == 1)
            showRegistrationNumber(registrationNumbers.get(0));
        else {
            ChoiceNumberDialog.create(registrationNumbers).show(getFragmentManager());
        }
    }

    @Override
    public void onError(Throwable throwable) {
        loadingDialog.dismiss();
        Snackbar.make(mETNumberOfPlate, throwable.getMessage(), Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void result(RegistrationNumber registrationNumber) {
        showRegistrationNumber(registrationNumber);
        super.result(registrationNumber);
    }

    public void showRegistrationNumber(RegistrationNumber number) {
        mETNumberOfPlate.setText(number.getNumber());
        mTVRegionOFPlate.setText(number.getRegion());
    }
}
