package angelectro.com.gooddrivers.activity;

import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Locale;

import angelectro.com.gooddrivers.R;
import angelectro.com.gooddrivers.UserChoiceEnum;
import angelectro.com.gooddrivers.content.RegistrationNumber;
import angelectro.com.gooddrivers.dialog.ChoiceNumberDialog;
import angelectro.com.gooddrivers.view.BaseView;
import angelectro.com.gooddrivers.voice_control.VoiceRecognition;

/**
 * Created by Zahit Talipov on 20.05.2016.
 */
public class BaseActivity extends AppCompatActivity implements TextToSpeech.OnInitListener, VoiceRecognition.Read, BaseView, ChoiceNumberDialog.ChoiceNumberListener {

    static TextToSpeech textToSpeech;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        VoiceRecognition.initialize(this, this);
        Intent checkTTSIntent = new Intent();
        checkTTSIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
        startActivityForResult(checkTTSIntent, VoiceRecognition.REQUEST_CODE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.listen) {
            VoiceRecognition.listenToSpeech();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == VoiceRecognition.REQUEST_CODE && resultCode == TextToSpeech.Engine.CHECK_VOICE_DATA_PASS) {
            if (textToSpeech == null)
                textToSpeech = new TextToSpeech(this, this);
        }
        VoiceRecognition.onActivityResult(requestCode, resultCode, data, null);
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {
            Log.d("textToSpeech", "init");
            textToSpeech.setLanguage(Locale.getDefault());//Язык
        }

    }

    @Override
    public void read(String text) {
        textToSpeech.setOnUtteranceProgressListener(null);
        Log.d("textToSpeech", text);
        textToSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, null);
        Toast.makeText(this, text, Toast.LENGTH_LONG).show();
    }

    @Override
    public void read(String text, UtteranceProgressListener progressListener) {
        Log.d("textToSpeech", text);
        textToSpeech.setOnUtteranceProgressListener(progressListener);
        HashMap<String, String> myHash = new HashMap<String, String>();
        myHash.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "SOME MESSAGE");
        textToSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, myHash);
        Toast.makeText(this, text, Toast.LENGTH_LONG).show();
    }

    @Override
    public void startAboutActivity(@NonNull RegistrationNumber registrationNumber) {
        AboutDriverActivity.start(this, registrationNumber.toString());
    }

    @Override
    public void startChoiceActionActivity(@NonNull UserChoiceEnum choiceEnum, @NonNull RegistrationNumber registrationNumber) {
        ChoiceActionActivity.startActivity(this, choiceEnum, registrationNumber.toString());
    }

    @Override
    public void result(RegistrationNumber registrationNumber) {
        startAboutActivity(registrationNumber);
    }
}
