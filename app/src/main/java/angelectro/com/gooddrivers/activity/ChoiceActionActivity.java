package angelectro.com.gooddrivers.activity;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.LinearLayout;

import java.util.List;

import angelectro.com.gooddrivers.R;
import angelectro.com.gooddrivers.Settings;
import angelectro.com.gooddrivers.UserChoiceEnum;
import angelectro.com.gooddrivers.adapters.PagerAdapterImpl;
import angelectro.com.gooddrivers.adapters.RawOrPanRWAdapter;
import angelectro.com.gooddrivers.content.Action;
import angelectro.com.gooddrivers.dialog.LoadingDialog;
import angelectro.com.gooddrivers.dialog.SignInDialog;
import angelectro.com.gooddrivers.fragment.ViewPagerExt;
import angelectro.com.gooddrivers.listeners.ConfirmButtonStateListener;
import angelectro.com.gooddrivers.listeners.OnItemClick;
import angelectro.com.gooddrivers.listeners.SignInCallback;
import angelectro.com.gooddrivers.presenter.ChoiceActionPresenter;
import angelectro.com.gooddrivers.presenter.impl.ChoiceActionPresenterFireBaseImpl;
import angelectro.com.gooddrivers.view.AboutDriverView;
import angelectro.com.gooddrivers.view.RewardOrPunishmentView;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Zahit Talipov on 30.03.2016.
 */
public class ChoiceActionActivity extends BaseActivity implements RewardOrPunishmentView,
        OnItemClick, View.OnClickListener, ConfirmButtonStateListener, SignInCallback {
    public static int CONFIRM_BUTTON_SHOW = 1;
    public static int CONFIRM_BUTTON_HIDE = 0;
    private static String NUMBER = "number";
    private static String USER_CHOICE = "choice";
    Action selectCurrentAction = null;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tabLayout)
    TabLayout tabLayout;
    @BindView(R.id.viewPager)
    ViewPagerExt viewPager;
    @BindView(R.id.layoutConfirm)
    LinearLayout linearLayoutConfirm;
    @BindView(R.id.buttonConfirm)
    Button buttonConfirm;
    SignInDialog signInDialog;
    ChoiceActionPresenter presenter;
    LoadingDialog loadingDialog = new LoadingDialog();
    PagerAdapterImpl pagerAdapter;
    UserChoiceEnum choiceEnum;

    public static void startActivity(@NonNull Context context, @NonNull UserChoiceEnum choiceEnum, @NonNull String registrationNumber) {
        Intent intent = new Intent(context, ChoiceActionActivity.class);
        intent.putExtra(USER_CHOICE, choiceEnum);
        intent.putExtra(NUMBER, registrationNumber);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.choise_activity);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        choiceEnum = (UserChoiceEnum) getIntent().getSerializableExtra(USER_CHOICE);
        setTitle(choiceEnum);
        presenter = new ChoiceActionPresenterFireBaseImpl(this);
        viewPagerInit();
        presenter.getList(this);

    }

    private void viewPagerInit() {
        pagerAdapter = new PagerAdapterImpl(getFragmentManager(), this, choiceEnum);
        viewPager.setAdapter(pagerAdapter);
        viewPager.setConfirmButtonStateListener(this);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

        }
        return super.onOptionsItemSelected(item);
    }

    public void setTitle(UserChoiceEnum choiceEnum) {
        switch (choiceEnum) {
            case Reward: {
                setTitle(getString(R.string.title_choise_reward));
                break;
            }
            case Punishment: {
                setTitle(getString(R.string.title_select_punishment));
                break;
            }
        }
    }

    @Override
    public void updateViewPager(List<Action> itemRPs) {
        pagerAdapter.updateList(itemRPs);
    }

    @Override
    public void showLoading() {
        loadingDialog.show(getFragmentManager());
    }

    @Override
    public void hideLoading() {
        loadingDialog.dismiss();
    }

    @Override
    public void showError() {

    }


    @Override
    public void onClick(Action action) {
        if (action.isSelected()) {
            selectCurrentAction = action;
            confirmButtonStateChanges(CONFIRM_BUTTON_SHOW);
        } else {
            selectCurrentAction = null;
            confirmButtonStateChanges(CONFIRM_BUTTON_HIDE);
        }
    }

    public void confirmButtonStateChanges(int state) {
        float height = 0;
        if (CONFIRM_BUTTON_HIDE == state) {
            if (selectCurrentAction != null) {
                selectCurrentAction.setSelected(false);
                getContentResolver().notifyChange(RawOrPanRWAdapter.uri, null);
            }
            height = linearLayoutConfirm.getHeight();
        }
        linearLayoutConfirm
                .animate()
                .translationY(height)
                .setInterpolator(new DecelerateInterpolator(2))
                .start();
    }

    @Override
    public void onClick(View v) {
        if (!Settings.isUserSignIn()) {
            signInDialog = new SignInDialog();
            signInDialog.show(getFragmentManager());
        } else {
            signIn();
        }

    }

    @Override
    public void signIn() {
        String registrationNumber = getIntent().getStringExtra(NUMBER);
        presenter.sendAction(selectCurrentAction, registrationNumber);
        finish();
    }
}
