package angelectro.com.gooddrivers.widjet;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;


/**
 * Created by Zahit Talipov on 06.04.2016.
 */
public class RewardOrPunishmentRecyclerView extends RecyclerView {
    public RewardOrPunishmentRecyclerView(Context context) {
        super(context);
    }

    public RewardOrPunishmentRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RewardOrPunishmentRecyclerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onAttachedToWindow() {
        Log.d("recycler","attached");
        super.onAttachedToWindow();
    }
}
