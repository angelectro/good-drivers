package angelectro.com.gooddrivers.content;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Zahit Talipov on 07.04.2016.
 */
public class Action {
    @SerializedName("type")
    private String type;
    @SerializedName("url_image")
    private String url_image;
    @SerializedName("category")
    private String category;
    @SerializedName("description")
    private String description;
    private boolean selected;

    public Action(){}

    public Action(String mType, String mImageURL, String mCategory, String mDescription) {
        this.type = mType;
        this.url_image = mImageURL;
        this.category = mCategory;
        this.description = mDescription;
    }

    public String getType() {
        return type;
    }

    public void setType(String mType) {
        this.type = mType;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getUrl_image() {
        return url_image;
    }

    public void setUrl_image(String mImageURL) {
        this.url_image = mImageURL;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String mCategory) {
        this.category = mCategory;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String mDescription) {
        this.description = mDescription;
    }

    @Override
    public String toString() {
        return "Action{" +
                "type='" + type + '\'' +
                ", url_image='" + url_image + '\'' +
                ", category='" + category + '\'' +
                ", description='" + description + '\'' +
                ", selected=" + selected +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Action action = (Action) o;
        if (type != null ? !type.equals(action.type) : action.type != null) return false;
        if (url_image != null ? !url_image.equals(action.url_image) : action.url_image != null)
            return false;
        if (category != null ? !category.equals(action.category) : action.category != null)
            return false;
        return description != null ? description.equals(action.description) : action.description == null;

    }

    @Override
    public int hashCode() {
        int result = type != null ? type.hashCode() : 0;
        result = 31 * result + (url_image != null ? url_image.hashCode() : 0);
        result = 31 * result + (category != null ? category.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }
}
