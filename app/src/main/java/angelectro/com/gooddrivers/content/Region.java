package angelectro.com.gooddrivers.content;

import java.io.Serializable;

/**
 * Created by Zahit Talipov on 11.05.2016.
 */
public class Region implements Serializable{
    String code;
    String region;

    public Region(){}

    public Region(String code, String region) {
        this.code = code;
        this.region = region;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }
}
