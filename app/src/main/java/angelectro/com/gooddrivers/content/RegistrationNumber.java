package angelectro.com.gooddrivers.content;

/**
 * Created by Zahit Talipov on 13.05.2016.
 */
public class RegistrationNumber {
    String number;
    String region;
    int quality;

    public static RegistrationNumber create(String number) {
        String[] temp = number.split(" ");
        RegistrationNumber registrationNumber = new RegistrationNumber();
        registrationNumber.setNumber(temp[0]);
        registrationNumber.setRegion(temp[1]);
        if (temp.length > 2)
            registrationNumber.setQuality(Integer.parseInt(temp[2].replace("%", "")));
        return registrationNumber;
    }

    public String forAssistant() {
        return String.format("%s %s %s %s", number.substring(0, 1), number.substring(1, 4), number.substring(4, 5), number.substring(5, number.length()));
    }

    @Override
    public String toString() {
        return number + " " + region;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RegistrationNumber number1 = (RegistrationNumber) o;

        if (quality != number1.quality) return false;
        if (number != null ? !number.equals(number1.number) : number1.number != null) return false;
        return region != null ? region.equals(number1.region) : number1.region == null;

    }

    @Override
    public int hashCode() {
        int result = number != null ? number.hashCode() : 0;
        result = 31 * result + (region != null ? region.hashCode() : 0);
        result = 31 * result + quality;
        return result;
    }

    public int getQuality() {
        return quality;
    }

    public void setQuality(int quality) {
        this.quality = quality;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }
}
