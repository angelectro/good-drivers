package angelectro.com.gooddrivers.observers;

import android.database.ContentObserver;
import android.os.Handler;
import android.os.Looper;

/**
 * Created by Zahit Talipov on 19.04.2016.
 */
public class DatabaseObserver extends ContentObserver {
    public DatabaseObserver() {
        super(new Handler(Looper.getMainLooper()));
    }

    @Override
    public boolean deliverSelfNotifications() {
        return super.deliverSelfNotifications();
    }

    @Override
    public void onChange(boolean selfChange) {
        super.onChange(selfChange);
    }
}
