package angelectro.com.gooddrivers.adapters;


import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v13.app.FragmentStatePagerAdapter;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import angelectro.com.gooddrivers.R;
import angelectro.com.gooddrivers.UserChoiceEnum;
import angelectro.com.gooddrivers.content.Action;
import angelectro.com.gooddrivers.fragment.CategoryFragment;

/**
 * Created by Zahit Talipov on 06.04.2016.
 */
public class PagerAdapterImpl extends FragmentStatePagerAdapter {
    public static Set<Action> rpList;
    String[] titles;
    Context context;
    UserChoiceEnum choiceEnum;

    public PagerAdapterImpl(FragmentManager manager, Context context, UserChoiceEnum choiceEnum) {
        super(manager);
        rpList = new HashSet<>();
        this.choiceEnum = choiceEnum;
        this.context = context;
        titles = context.getResources().getStringArray(R.array.categories);
    }

    @Override
    public int getCount() {
        return titles.length;
    }

    public void updateList(List<Action> itemRPs) {
        rpList.addAll(itemRPs);
        context.getContentResolver().notifyChange(RawOrPanRWAdapter.uri,null);
    }


    @Override
    public Fragment getItem(int position) {
        Log.d("position", String.valueOf(position));
        return CategoryFragment.createFragment(choiceEnum, position == 0 ? null : titles[position]);
    }


    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
    }


}
