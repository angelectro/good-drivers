package angelectro.com.gooddrivers.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import angelectro.com.gooddrivers.R;
import angelectro.com.gooddrivers.listeners.OnItemClickRegion;
import angelectro.com.gooddrivers.content.Region;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Zahit Talipov on 11.05.2016.
 */
public class RegionsAdapter extends RecyclerView.Adapter<RegionsAdapter.Holder> {

    List<Region> mRegions = new ArrayList<>();
    OnItemClickRegion onItemClick;

    public RegionsAdapter(OnItemClickRegion onItemClick) {
        this.onItemClick = onItemClick;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new Holder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_region, parent, false));
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        Region region = mRegions.get(position);
        holder.mCode.setText(region.getCode());
        holder.mRegion.setText(region.getRegion());
    }

    @Override
    public int getItemCount() {
        return mRegions.size();
    }

    public void update(List<Region> regions) {
        mRegions.clear();
        mRegions.addAll(regions);
        notifyDataSetChanged();
    }

    class Holder extends RecyclerView.ViewHolder {
        @BindView(R.id.textViewCode)
        TextView mCode;
        @BindView(R.id.textViewRegion)
        TextView mRegion;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener((v) -> onItemClick.onClickItem(mRegions.get(getLayoutPosition())));
        }
    }
}
