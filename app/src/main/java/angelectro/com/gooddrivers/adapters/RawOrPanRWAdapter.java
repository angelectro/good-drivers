package angelectro.com.gooddrivers.adapters;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import angelectro.com.gooddrivers.ListObserver;
import angelectro.com.gooddrivers.R;
import angelectro.com.gooddrivers.UserChoiceEnum;
import angelectro.com.gooddrivers.content.Action;
import angelectro.com.gooddrivers.listeners.OnItemClick;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Zahit Talipov on 07.04.2016.
 */
public class RawOrPanRWAdapter extends RecyclerView.Adapter<RawOrPanRWAdapter.Holder> {
    public static Uri uri = Uri.parse("content://" + RawOrPanRWAdapter.class.getSimpleName());
    Set<Action> listBase;
    List<Action> listForCurrent;
    Context mContext;
    UserChoiceEnum mChoiceEnum;
    OnItemClick onItemClick;
    String mCategory;
    ListObserver listObserver = new ListObserver() {
        @Override
        public void onChange(boolean selfChange) {
            super.onChange(selfChange);
            query();
            notifyDataSetChanged();
        }
    };

    public RawOrPanRWAdapter(Context context, UserChoiceEnum choiceEnum, String category) {
        mContext = context;
        mChoiceEnum = choiceEnum;
        mCategory = category;
        listBase = PagerAdapterImpl.rpList;
        query();
        context.getContentResolver().registerContentObserver(uri, true, listObserver);
    }

    public void setOnItemClick(OnItemClick onItemClick) {
        this.onItemClick = onItemClick;
    }

    private void query() {
        listForCurrent = new ArrayList<>();
        if (mCategory != null) {
            Iterator<Action> iterator = listBase.iterator();
            while (iterator.hasNext()) {
                Action itemRP = iterator.next();
                if (itemRP.getCategory().contains(mCategory)
                        && itemRP.getType().contains(mChoiceEnum.toString())) {
                    Log.d("iter", itemRP.toString());
                    listForCurrent.add(itemRP);
                }
            }
        } else {
            Iterator<Action> iterator = listBase.iterator();
            while (iterator.hasNext()) {
                Action itemRP = iterator.next();
                if (itemRP.getType().contains(mChoiceEnum.toString())) {
                    Log.d("iter", itemRP.toString());
                    listForCurrent.add(itemRP);
                }
            }
        }

    }


    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.d("query", "createViewHolder");
        View view = null;
        switch (mChoiceEnum) {
            case Reward: {
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.choise_item_good, parent, false);
                break;
            }
            case Punishment: {
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.choise_item_bad, parent, false);
                break;
            }
        }
        return new Holder(view);
    }


    @Override
    public void onBindViewHolder(Holder holder, int position) {
        Action itemRP = listForCurrent.get(position);
        holder.mTVDescription.setText(itemRP.getDescription());
        holder.mTVCategory.setText(itemRP.getCategory());
        Picasso.with(mContext)
                .load(itemRP.getUrl_image()).resize(200, 200)
                .into(holder.mImageIcon);
        if (itemRP.isSelected()) {
            holder.mCardView.setBackgroundColor(Color.parseColor("#eee1e0e0"));
        } else {
            holder.mCardView.setBackgroundColor(Color.WHITE);
        }
    }


    @Override
    public int getItemCount() {
        return listForCurrent.size();
    }


    public void setSelected(int position) {
        Action action = listForCurrent.get(position);
        boolean isSelected = false;
        for (Action itemRP : listBase) {
            if (itemRP.isSelected()) {
                isSelected = true;
                if (itemRP == action) {
                    itemRP.setSelected(!itemRP.isSelected());
                    break;
                } else {
                    itemRP.setSelected(false);
                    action.setSelected(true);
                    break;
                }
            }
        }

        if (!isSelected)
            action.setSelected(true);
        mContext.getContentResolver().notifyChange(uri, null);
        onItemClick.onClick(action);
        notifyDataSetChanged();
    }


    class Holder extends RecyclerView.ViewHolder {
        @BindView(R.id.imageIcon)
        ImageView mImageIcon;
        @BindView(R.id.tvCategory)
        TextView mTVCategory;
        @BindView(R.id.tvDescription)
        TextView mTVDescription;
        @BindView(R.id.cardView)
        CardView mCardView;
        View view;

        public Holder(View itemView) {
            super(itemView);
            view = itemView;
            itemView.setClickable(true);
            ButterKnife.bind(this, view);
            itemView.setOnClickListener(v ->
                    setSelected(getLayoutPosition()));
        }
    }
}
