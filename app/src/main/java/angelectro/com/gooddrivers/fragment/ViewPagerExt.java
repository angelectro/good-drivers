package angelectro.com.gooddrivers.fragment;


import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;

import angelectro.com.gooddrivers.activity.ChoiceActionActivity;
import angelectro.com.gooddrivers.listeners.ConfirmButtonStateListener;
import angelectro.com.gooddrivers.listeners.PageStateChangeListener;

/**
 * Created by Zahit Talipov on 15.05.2016.
 */
public class ViewPagerExt extends ViewPager {

    ConfirmButtonStateListener confirmButtonStateListener;
    PageStateChangeListener changeListener = new PageStateChangeListener() {
        @Override
        public void onPageSelected(int position) {
            confirmButtonStateListener.confirmButtonStateChanges(ChoiceActionActivity.CONFIRM_BUTTON_HIDE);
        }
    };

    public ViewPagerExt(Context context) {
        super(context);
        init();
    }

    public ViewPagerExt(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public void setConfirmButtonStateListener(ConfirmButtonStateListener confirmButtonStateListener) {
        this.confirmButtonStateListener = confirmButtonStateListener;
    }

    private void init() {
        setOnPageChangeListener(changeListener);
    }

}
