package angelectro.com.gooddrivers.fragment;


import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import angelectro.com.gooddrivers.R;
import angelectro.com.gooddrivers.UserChoiceEnum;
import angelectro.com.gooddrivers.listeners.OnItemClick;
import angelectro.com.gooddrivers.adapters.RawOrPanRWAdapter;
import angelectro.com.gooddrivers.widjet.RewardOrPunishmentRecyclerView;

/**
 * Created by Zahit Talipov on 08.04.2016.
 */
public class CategoryFragment extends Fragment {
    private static String CATEGORY_KEY = "category";
    private static String CHOICE_KEY = "choice";
    private View mViewRoot;
    private OnItemClick mOnItemClick;

    public static CategoryFragment createFragment(UserChoiceEnum choiceEnum, String category) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(CHOICE_KEY, choiceEnum);
        bundle.putString(CATEGORY_KEY, category);
        CategoryFragment categoryFragment = new CategoryFragment();
        categoryFragment.setArguments(bundle);
        return categoryFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mViewRoot = (View) inflater.inflate(R.layout.recycler_view, null, false);
        initRecyclerView();
        return mViewRoot;
    }

    public void initRecyclerView() {
        Bundle bundle = getArguments();
        String category = bundle.getString(CATEGORY_KEY);
        UserChoiceEnum choiceEnum = (UserChoiceEnum) bundle.getSerializable(CHOICE_KEY);
        RewardOrPunishmentRecyclerView
                view = (RewardOrPunishmentRecyclerView) mViewRoot.findViewById(R.id.recyclerV);
        RawOrPanRWAdapter rawOrPanRWAdapter = new RawOrPanRWAdapter(getActivity(), choiceEnum, category);
        rawOrPanRWAdapter.setOnItemClick((OnItemClick) getActivity());
        view.setLayoutManager(new LinearLayoutManager(getActivity()));
        view.setAdapter(rawOrPanRWAdapter);
    }
}
