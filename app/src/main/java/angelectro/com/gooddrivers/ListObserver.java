package angelectro.com.gooddrivers;

import android.database.ContentObserver;
import android.os.Handler;
import android.os.Looper;

/**
 * Created by Zahit Talipov on 04.05.2016.
 */
public class ListObserver extends ContentObserver {
    public ListObserver() {
        super(new Handler(Looper.getMainLooper()));
    }
}
