package angelectro.com.gooddrivers;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKSdk;

/**
 * Created by Zahit Talipov on 09.05.2016.
 */
public class Settings {
    private static Context mContext;
    private static String NAME = "good_drivers";
    private static String TOKEN = "token";
    private static String USER_ID = "user_id";
    private static String SOCIAL = "social";
    private static String SOCIAL_VK = "vk";
    private static String SOCIAL_FB = "fb";

    public static void initialize(Context context) {
        mContext = context;
    }

    private static SharedPreferences getPreferences() {
        return mContext.getSharedPreferences(NAME, Context.MODE_PRIVATE);
    }

    public static String getTypeSocial() {
        return getPreferences().getString(SOCIAL, "");
    }

    //сохрание профиля Facebook
    public static void saveUser(LoginResult loginResult) {
        SharedPreferences.Editor editor = getPreferences().edit();
        editor.putString(TOKEN, loginResult.getAccessToken().getToken());
        editor.putString(SOCIAL, SOCIAL_FB);
        editor.putString(USER_ID, loginResult.getAccessToken().getUserId());
        editor.commit();
    }

    //сохрание профиля Вконтакте
    public static void saveUser(VKAccessToken vkAccessToken) {
        SharedPreferences.Editor editor = getPreferences().edit();
        editor.putString(TOKEN, vkAccessToken.accessToken);
        editor.putString(SOCIAL, SOCIAL_VK);
        editor.putString(USER_ID, vkAccessToken.userId);
        editor.commit();
    }

    public static boolean isUserSignIn() {
        return !TextUtils.isEmpty(getPreferences().getString(TOKEN, ""));
    }

    //выход
    public static void logout() {
        String social = getPreferences().getString(SOCIAL, "");
        if (social.contains(SOCIAL_FB)) {
            LoginManager.getInstance().logOut();
        } else if (social.contains(SOCIAL_VK)) {
            VKSdk.logout();
        }
        SharedPreferences.Editor editor = getPreferences().edit();
        editor.remove(TOKEN);
        editor.remove(SOCIAL);
        editor.remove(USER_ID);
        editor.commit();
    }
}
