package angelectro.com.gooddrivers.voice_control;

import android.app.Activity;
import android.content.Intent;
import android.speech.RecognizerIntent;
import android.speech.tts.UtteranceProgressListener;
import android.util.Log;

import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import angelectro.com.gooddrivers.AppDelegate;
import angelectro.com.gooddrivers.FireBaseData;
import angelectro.com.gooddrivers.R;
import angelectro.com.gooddrivers.UserChoiceEnum;
import angelectro.com.gooddrivers.content.Action;
import angelectro.com.gooddrivers.content.RegistrationNumber;
import angelectro.com.gooddrivers.database.SQLite;
import angelectro.com.gooddrivers.dialog.ChoiceNumberDialog;
import angelectro.com.gooddrivers.listeners.CallbackResult;
import angelectro.com.gooddrivers.listeners.ConfirmCallback;
import angelectro.com.gooddrivers.view.BaseView;

/**
 * Created by Zahit Talipov on 20.05.2016.
 */
public class VoiceRecognition {
    public static int REQUEST_CODE = 23;
    static Activity mActivity;
    static Read mRead;
    static BaseView baseView;
    static ConfirmCallback mCallbackConfirm;

    public static void listenToSpeech() {
        Intent listenIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        listenIntent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, VoiceRecognition.class.getClass().getPackage().getName());
        listenIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        listenIntent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 10);
        mActivity.startActivityForResult(listenIntent, REQUEST_CODE);
    }

    public static void initialize(Activity activity, Read read) {
        mActivity = activity;
        baseView = (BaseView) activity;
        mRead = read;
    }

    public static boolean onActivityResult(int requestCode, int resultCode, Intent data, CallbackResult<ArrayList<String>> callbackResult) {
        if (requestCode == REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            ArrayList<String> suggestedWords = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            String[] regexs = mActivity.getResources().getStringArray(R.array.matchers);
            int count_matches = 0;
            boolean isMatches = false;
            int j = 0;
            int currentMatch = 0;
            List<Matcher> matchers = new ArrayList<>();
            for (String regex : regexs) {
                j++;
                isMatches = false;
                for (String s : suggestedWords) {
                    Log.d("result", s);
                    Pattern pattern = Pattern.compile(regex);
                    Matcher matcher = pattern.matcher(s);
                    if (matcher.matches()) {
                        matchers.add(matcher);
                        isMatches = true;
                    }
                }
                if (isMatches) {
                    count_matches++;
                    currentMatch = j;
                }
            }
            if (count_matches == 1) {
                switch (currentMatch) {
                    case 1: {
                        regNumbers(matchers);
                        break;
                    }
                    case 2: {
                        mRead.read("привет");
                        break;
                    }
                    case 3: {
                        showListActions(matchers);
                        break;
                    }
                    case 4: {
                        sendAction(matchers);
                        break;
                    }
                    case 5: {
                        if (mCallbackConfirm != null) {
                            mCallbackConfirm.confirmed();
                            mCallbackConfirm = null;
                        }
                        break;
                    }
                    case 6: {
                        if (mCallbackConfirm != null) {
                            mCallbackConfirm.rejection();
                            mCallbackConfirm = null;
                        }
                        break;
                    }
                }

            } else {
                incorrect();
            }
            return true;
        }
        return false;
    }

    private static void regNumbers(List<Matcher> matchers) {
        Set<RegistrationNumber> setNumbers = new HashSet<>();
        for (Matcher matcher : matchers) {
            RegistrationNumber number = new RegistrationNumber();
            number.setNumber(new String());
            for (int i = 1; i <= matcher.groupCount() && matcher.matches(); i++) {
                String group = matcher.group(i).toUpperCase();
                Log.d("result", group);
                if (i == 2)
                    number.setNumber(number.getNumber().concat(group));
                else if (i == 5)
                    number.setRegion(group);
                else {
                    number.setNumber(number.getNumber().concat(String.valueOf(group.charAt(0))));
                }
            }
            Log.d("result", number.toString());
            setNumbers.add(number);
        }
        List<RegistrationNumber> registrationNumbers = new ArrayList<>(setNumbers);
        if (registrationNumbers.size() == 1) {
            baseView.startAboutActivity(registrationNumbers.get(0));
        } else {
            mRead.read(mActivity.getString(R.string.text_result_of_multiple_numbers));
            ChoiceNumberDialog.create(registrationNumbers).show(mActivity.getFragmentManager());
        }
    }

    private static void sendAction(List<Matcher> matchers) {
        if (AppDelegate.currentRegistrationNumber != null) {
            Action action = null;
            for (Matcher matcher : matchers) {
                for (int i = 1; i <= matcher.groupCount() && matcher.matches(); i++) {
                    if (i == 2) {
                        String s = matcher.group(i).replace("1", "первый");
                        s = s.substring(0, 1).toUpperCase() + s.substring(1).toLowerCase();
                        List<Action> actions = SQLite.getAllActions(s);
                        if (!actions.isEmpty()) {
                            action = actions.get(0);
                            break;
                        }
                    }
                }
                if (action != null)
                    break;
            }
            if (action != null) {
                if (AppDelegate.currentRegistrationNumber != null) {
                    final Action finalAction = action;
                    mCallbackConfirm = new ConfirmCallback() {
                        @Override
                        public void confirmed() {
                            FireBaseData.getRegistrationNumbers().child(AppDelegate.currentRegistrationNumber.toString()).push().setValue(finalAction, new Firebase.CompletionListener() {
                                @Override
                                public void onComplete(FirebaseError firebaseError, Firebase firebase) {
                                    Log.d("firebase", "complite");
                                }
                            });
                        }

                        @Override
                        public void rejection() {

                        }
                    };
                    mRead.read(String.format("Отправить %s на номер %s. Я вас верно поняла?",
                            action.getDescription(),
                            AppDelegate.currentRegistrationNumber.forAssistant()),
                            new UtteranceProgressListener() {
                                @Override
                                public void onStart(String utteranceId) {

                                }

                                @Override
                                public void onDone(String utteranceId) {
                                    listenToSpeech();
                                }

                                @Override
                                public void onError(String utteranceId) {

                                }
                            });

                } else {
                    mRead.read(mActivity.getString(R.string.text_not_registr_number));
                }
            } else
                incorrect();
        } else
            mRead.read(mActivity.getString(R.string.text_not_registr_number));
    }

    private static void showListActions(List<Matcher> matchers) {
        if (AppDelegate.currentRegistrationNumber != null)
            for (Matcher matcher : matchers) {
                String group = matcher.group(1);
                if (group.toLowerCase().startsWith("награ")) {
                    baseView.startChoiceActionActivity(UserChoiceEnum.Reward, AppDelegate.currentRegistrationNumber);
                } else if (group.toLowerCase().startsWith("наказ")) {
                    baseView.startChoiceActionActivity(UserChoiceEnum.Punishment, AppDelegate.currentRegistrationNumber);
                } else {
                    incorrect();
                }
            }
        else
            mRead.read(mActivity.getString(R.string.text_not_registr_number));

    }

    private static void incorrect() {
        mRead.read(mActivity.getString(R.string.text_not_understand));
    }


    public interface Read {
        void read(String text);
        public void read(String text, UtteranceProgressListener progressListener);
    }
}
  