package angelectro.com.gooddrivers.listeners;

/**
 * Created by Zahit Talipov on 27.05.2016.
 */
public interface ConfirmCallback {
    public void confirmed();
    public void rejection();
}
