package angelectro.com.gooddrivers.listeners;

import angelectro.com.gooddrivers.content.Action;

public interface OnItemClick{
    void onClick(Action action);
}
