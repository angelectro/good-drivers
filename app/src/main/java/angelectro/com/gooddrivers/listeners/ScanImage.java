package angelectro.com.gooddrivers.listeners;

import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.util.List;

import angelectro.com.gooddrivers.content.RegistrationNumber;

/**
 * Created by Zahit Talipov on 12.05.2016.
 */
public abstract class ScanImage {
    public static File directory;
    public static Uri uriImage;
    public static final int REQUEST_CODE_PHOTO = 2;
    public CallbackResult<List<RegistrationNumber>> mCallbackScanPhoto;

    public abstract boolean onActivityResult(int requestCode, int resultCode, Intent intent,CallbackResult callbackScanPhoto);

    public abstract void uploadFile(Uri uri);

    public static Uri generateFileUri() {
        createDirectory();
        File file = new File(directory.getPath() + "/" + "photo_"
                + System.currentTimeMillis() + ".jpg");
        Log.d("photo", "fileName = " + file);
        uriImage = Uri.fromFile(file);
        return uriImage;
    }

    private static void createDirectory() {
        directory = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                "good");
        if (!directory.exists())
            directory.mkdirs();
    }
}
