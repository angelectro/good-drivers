package angelectro.com.gooddrivers.listeners;

import angelectro.com.gooddrivers.content.Region;

public interface OnItemClickRegion {
    void onClickItem(Region region);
}
