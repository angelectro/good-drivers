package angelectro.com.gooddrivers.listeners.impl;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import angelectro.com.gooddrivers.api.ApiFactory;
import angelectro.com.gooddrivers.content.RegistrationNumber;
import angelectro.com.gooddrivers.listeners.CallbackResult;
import angelectro.com.gooddrivers.listeners.ScanImage;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Zahit Talipov on 12.05.2016.
 */
public class ScanImageImpl extends ScanImage {
    Context mContext;

    public ScanImageImpl(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    public boolean onActivityResult(int requestCode, int resultCode, Intent data, CallbackResult callbackScanPhoto) {
        mCallbackScanPhoto = callbackScanPhoto;
        if (REQUEST_CODE_PHOTO == requestCode && resultCode == Activity.RESULT_OK) {
            uploadFile(uriImage);
            return true;
        }
        return false;
    }

    @Override
    public void uploadFile(Uri fileUri) {
        File file = new File(fileUri.getPath());
        RequestBody requestFile =
                RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("pic", file.getName(), requestFile);

        Log.d("Upload", "Upload file size:" + file.length());
        Call<ResponseBody> call = ApiFactory.getScanPhotoService().uploadImage(body);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    List<RegistrationNumber> registrationNumbers = new ArrayList<RegistrationNumber>();
                    String[] numbers = response.body().string().split("\r\n");
                    if (!numbers[0].contains("not found")) {
                        for (String number : numbers) {
                            registrationNumbers.add(RegistrationNumber.create(transliteration(number)));
                        }
                    }
                    mCallbackScanPhoto.onResult(registrationNumbers);
                    Log.d("upload", numbers[0] + " wef");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                mCallbackScanPhoto.onError(t);
                Log.d("upload", t.getMessage());

            }
        });
    }

    public String transliteration(String m) {
        Log.d("xz", "transliteration");
        return m.replace("A", "А")
                .replace("C", "С")
                .replace("E", "Е")
                .replace("T", "Т")
                .replace("H", "Н")
                .replace("O", "О")
                .replace("P", "Р")
                .replace("K", "К")
                .replace("X", "Х")
                .replace("B", "В")
                .replace("M", "М")
                .replace("Y", "У");
    }
}
