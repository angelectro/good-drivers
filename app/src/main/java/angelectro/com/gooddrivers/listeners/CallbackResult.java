package angelectro.com.gooddrivers.listeners;

import java.util.List;

import angelectro.com.gooddrivers.content.RegistrationNumber;

/**
 * Created by Zahit Talipov on 13.05.2016.
 */
public interface CallbackResult<T> {

    public void onResult(T result);

    public void onError(Throwable throwable);
}
