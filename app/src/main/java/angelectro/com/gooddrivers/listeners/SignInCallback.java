package angelectro.com.gooddrivers.listeners;

/**
 * Created by Zahit Talipov on 15.05.2016.
 */
public interface SignInCallback {
    public void signIn();
}
