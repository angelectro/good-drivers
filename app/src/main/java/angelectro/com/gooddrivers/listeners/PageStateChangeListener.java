package angelectro.com.gooddrivers.listeners;

import android.support.v4.view.ViewPager;

/**
 * Created by Zahit Talipov on 15.05.2016.
 */
public abstract class PageStateChangeListener implements ViewPager.OnPageChangeListener {
    @Override
    public void onPageScrollStateChanged(int state){

    };

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels){

    };
}
