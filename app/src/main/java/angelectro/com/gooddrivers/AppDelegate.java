package angelectro.com.gooddrivers;

import android.app.Application;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.firebase.client.Firebase;
import com.vk.sdk.VKSdk;

import angelectro.com.gooddrivers.content.RegistrationNumber;
import angelectro.com.gooddrivers.database.SQLite;

/**
 * Created by Zahit Talipov on 19.04.2016.
 */
public class AppDelegate extends Application {

    public static RegistrationNumber currentRegistrationNumber;

    @Override
    public void onCreate() {
        super.onCreate();
        SQLite.initialize(this);
        VKSdk.initialize(this);
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        Firebase.setAndroidContext(this);
        Settings.initialize(this);
    }
}
